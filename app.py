from typing import Any, Union

from flask import Flask, request, json, Response, make_response
from flask.helpers import make_response
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import ValidationError
from flask_restful import Api, Resource

app = Flask(__name__)

app.config.from_pyfile("config.py")

db = SQLAlchemy(app)
ma = Marshmallow(app)


# Using inheritance. Could also use dataclasses map via the registry.mapped() decorator: https://docs.sqlalchemy.org/en/14/orm/mapping_styles.html#declarative-mapping-with-dataclasses-and-attrs
class Satellite(db.Model):
    __tablename__ = "satellite"
    id = db.Column(db.Integer, primary_key=True)
    designator = db.Column(db.String(11))
    name = db.Column(db.String(255))

    def __repr__(self):
        return f"<Sat {self.id} {self.designator} {self.name}>"


class SatelliteSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Satellite
        load_instance = True


satellite_schema = SatelliteSchema()
satellites_schema = SatelliteSchema(many=True)


#########
# Basic
#########


@app.route("/")
def hello_world():
    return "Hello Universe"


#########
# Function-based ~/sats/
#########


@app.route("/sats/")
def list() -> Any:
    all_sats = Satellite.query.all()
    return satellites_schema.jsonify(all_sats)


@app.route("/sats/", methods=["POST"])
def create() -> tuple[Any, int]:
    designator = request.json.get("designator", "")
    name = request.json.get("name", "")

    new_sat = Satellite(designator=designator, name=name)

    db.session.add(new_sat)
    db.session.commit()

    return satellite_schema.jsonify(new_sat), 201


@app.route("/sats/<int:sat_id>/", methods=["GET"])
def get(sat_id: str) -> Any:
    sat = Satellite.query.get_or_404(sat_id)
    return satellite_schema.jsonify(sat)


@app.route("/sats/<int:sat_id>/", methods=["PATCH"])
def patch(sat_id: str) -> Any:
    sat = Satellite.query.get_or_404(sat_id)

    if "designator" in request.json:
        sat.designator = request.json["designator"]
    if "name" in request.json:
        sat.name = request.json["name"]

    db.session.add(sat)
    db.session.commit()
    return satellite_schema.jsonify(sat)


@app.route("/sats/<int:sat_id>/", methods=["DELETE"])
def delete(sat_id: str) -> tuple[dict, int]:
    sat = Satellite.query.get_or_404(sat_id)
    db.session.delete(sat)
    db.session.commit()
    return {}, 204


#########
# Class/ Resource based ~/satellites/
#########


class SatelliteListResource(Resource):
    def get(self) -> list:
        satellites = Satellite.query.all()
        return satellites_schema.dump(satellites)

    def post(self) -> tuple[dict, int]:
        try:
            satellite = satellite_schema.load(request.json)
        except ValidationError as e:
            return e.messages, 400

        db.session.add(satellite)
        db.session.commit()
        return satellite_schema.dump(satellite), 201


class SatelliteResource(Resource):
    def get(self, satellite_id: str) -> dict:
        satellite = Satellite.query.get_or_404(satellite_id)
        return satellite_schema.dump(satellite)

    def patch(self, satellite_id: str) -> Union[dict, tuple[dict, int]]:
        errs = satellite_schema.validate(request.json)
        if errs:
            return errs, 400

        satellite = Satellite.query.get_or_404(satellite_id)

        if "designator" in request.json:
            satellite.designator = request.json["designator"]
        if "name" in request.json:
            satellite.name = request.json["name"]

        db.session.commit()
        return satellite_schema.dump(satellite)

    def delete(self, satellite_id: str) -> tuple[dict, int]:
        satellite = Satellite.query.get_or_404(satellite_id)
        db.session.delete(satellite)
        db.session.commit()
        return {}, 204


api = Api(app)
api.add_resource(SatelliteListResource, "/satellites/")
api.add_resource(SatelliteResource, "/satellites/<int:satellite_id>")


if __name__ == "__main__":
    app.run()