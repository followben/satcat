# Basic Flask REST API with SQLAlchemy, Marshmallow & Postgres

Supports two endpoints that list, create, get, update and delete a database of known satellites:
- `~/sats/` that uses route functions to maintain data
- `~/satellites/` that a class-based approach via flask-restful

Initial data import is currently done via a python notebook.